extends Spatial

var end_explosion = load("res://Effects/EndExplosion/EndExplosion.tscn")
var ring = load("res://Effects/Ring/Ring.tscn")

var EFFECT_END_EXPLOSION = 1
var EFFECT_RING = 2

func create_effect(type : int = 0, global_origin := Vector3()):
	var effect : Spatial
	
	match type:
		0:
			return
		1:
			effect = end_explosion.instance()
		2:
			effect = ring.instance()
	
	add_child(effect)
	effect.global_transform.origin = global_origin
	
	return effect
