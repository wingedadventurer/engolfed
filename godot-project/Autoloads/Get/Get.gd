extends Node

func Cam() -> Cam:
	var nodes = get_tree().get_nodes_in_group("cam")
	for node in nodes:
		if node.current:
			return node
	
	return null

func UI() -> UI:
	var nodes = get_tree().get_nodes_in_group("ui")
	if nodes.size() > 0: return nodes[0]
	else: return null

func Player():
	var nodes = get_tree().get_nodes_in_group("golf_ball")
	if nodes.size() > 0: return nodes[0]
	else: return null
