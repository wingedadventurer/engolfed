extends Node

var TYPE_GAME = 1
var TYPE_MENU = 2

func _ready():
	Music.play(Music.TYPE_GAME)

func play(type : int):
	stop_all_music()
	match type:
		TYPE_GAME:
			$Game.play()
		TYPE_MENU:
			$Menu.play()

func stop_all_music():
	for child in get_children():
		if child is AudioStreamPlayer:
			child.stop()
