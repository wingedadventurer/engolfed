extends Spatial

export (float) var orbit_speed = 2.0

func _process(delta):
	rotate_y(deg2rad(orbit_speed) * delta)
