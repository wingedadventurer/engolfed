extends Spatial

signal end

func _ready():
	$Vortex.visible = true
	$OutsideAnim.play("outsides")
	$InsideAnim.play("insides")

func _on_End_body_entered(body):
	if body.is_in_group("golf_ball"):
		var effect = Effect.create_effect(Effect.EFFECT_END_EXPLOSION, global_transform.origin)
		effect.scale = scale * 0.2
		body.die()
		emit_signal("end")
		$EndSFX.play()
		$Ambience.stop()
		
		$Tween.interpolate_property(self, "scale", scale, Vector3.ZERO, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		$Tween.start()
		
		Get.Cam().shake(2.0, 0.2)
		Get.Cam().end = true
