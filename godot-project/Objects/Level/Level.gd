extends Spatial
class_name Level

var END_TIME = 5.0

var cam_scene = load("res://Objects/Cam/Cam.tscn")
var main_menu_scene = load("res://UI/MainMenu/MainMenu.tscn")

var cam : Spatial = null

func _ready():
	# instancing and preparing camera
	cam = cam_scene.instance()
	add_child(cam)
	cam.global_transform.origin = $GolfBall.global_transform.origin
	cam.follow_target = $GolfBall
	cam.target = $GolfBall/CamPivot/CamPos
	$GolfBall.cam = cam
	
	# connecting black holes
	for node in get_tree().get_nodes_in_group("black_hole"):
		node.connect("end", self, "_on_BlackHole_end")
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):
	if Input.is_action_just_pressed("reset"):
		get_tree().reload_current_scene()

func _on_BlackHole_end():
	var shade = $UI/EndShade
	$EndTween.interpolate_property(shade, "color:a", shade.color.a, 1.0, END_TIME, Tween.TRANS_QUAD, Tween.EASE_IN)
	$EndTween.start()

func _on_EndTween_tween_completed(object, key):
	get_tree().change_scene_to(main_menu_scene)
