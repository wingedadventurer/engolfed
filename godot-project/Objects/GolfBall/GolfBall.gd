extends RigidBody

var REST_VELOCITY_THRESHOLD = 0.25
var AIM_SPIN_SPEED = 0.1
var IMPACT_VELOCITY_SOUND_THRESHOLD_MIN = 1.0
var IMPACT_VELOCITY_SOUND_THRESHOLD_MAX = 5.0
export var MAX_charge = 30.0

var MIN_CROSSHAIR_ALPHA_DISTANCE = 0.1
var MAX_CROSSHAIR_ALPHA_DISTANCE = 0.5

var STATE_AIMING = 1
var STATE_MOVING = 2

var camera_zoom = 0.0 setget set_camera_zoom
var MAX_CAMERA_ZOOM = 8.0
var CAMERA_ZOOM_STEP = 2

var planet : StaticBody = null
var state = STATE_MOVING
var aim_start
var aim_rotation = 0.0
var aim_height = 0.0

var cam : Cam = null

var skip_shot = true

func _ready():
	$Trail.visible = true
	$Trail.emitting = true
	$CamPivot.set_as_toplevel(true)

func _process(delta):
	# camera placement
	$CamPivot.transform.origin = transform.origin
	
	update_ui()

func _physics_process(delta):
	# avoid cam getting inside bodies
	var cast : RayCast = $CamPivot/CamCast
	cast.cast_to = Vector3(0, 0, camera_zoom)
	if cast.is_colliding():
		var length = (cast.get_collision_point() - global_transform.origin).length()
		$CamPivot/CamPos.translation.z = length * 0.8
	else:
		$CamPivot/CamPos.translation.z = camera_zoom
	
	match state:
		STATE_AIMING:
			state_aiming(delta)
		STATE_MOVING:
			state_moving(delta)

func set_camera_zoom(value):
	camera_zoom = clamp(value, 0, MAX_CAMERA_ZOOM)
	
	$CamPivot/CamPos.translation.z = camera_zoom

func update_ui():
	var ui = Get.UI()
	if not ui: return
	
	# velocity label
	if not ui: return
	var label = str(get_linear_velocity().length()).pad_decimals(3)
	ui.update_velocity(label)
	
	# crosshair alpha
	var distance = (global_transform.origin - Get.Cam().global_transform.origin).length()
	var crosshair = ui.get_node("Crosshair")
	var value
	if distance > MAX_CROSSHAIR_ALPHA_DISTANCE: value = 0.0
	elif distance < MIN_CROSSHAIR_ALPHA_DISTANCE: value = 1.0
	else: value = (1 - (distance - MIN_CROSSHAIR_ALPHA_DISTANCE) / (MAX_CROSSHAIR_ALPHA_DISTANCE - MIN_CROSSHAIR_ALPHA_DISTANCE))
	ui.update_crosshair_alpha(value)

func state_aiming(delta):
	# camera rotation
	var normal = get_planet_normal()
	$CamPivot.look_at(global_transform.origin + aim_start, normal)
	$CamPivot.rotate(normal, aim_rotation)
	$CamPivot.rotate($CamPivot.transform.basis.x, aim_height)
	
	# shooting
	if skip_shot:
		skip_shot = false
		return
	
	if Input.is_action_just_pressed("shoot"):
		$ChargeTime.start()
	
	if Input.is_action_just_released("shoot"):
		var charge = (1.0 - $ChargeTime.time_left) * MAX_charge
		var aim = -$CamPivot.transform.basis.z
		shoot(aim, charge)
		$ChargeTime.stop()
		change_state(STATE_MOVING)
	
	# updating UI shot charge value
	if not $ChargeTime.is_stopped():
		var charge = (1.0 - $ChargeTime.time_left)
		Get.UI().update_charge_value(charge)

func state_moving(delta):
	# checking for rest
	if linear_velocity.length() < REST_VELOCITY_THRESHOLD:
		if $MinimumRestTime.is_stopped():
			$MinimumRestTime.start()
	else:
		if not $MinimumRestTime.is_stopped():
			$MinimumRestTime.stop()

func change_state(new_state):
	match new_state:
		STATE_AIMING:
			# calculating new aim start
			var normal : Vector3 = get_planet_normal()
#			var temp : Vector3 = normal.rotated(Vector3.UP, 0.1).rotated(Vector3.LEFT, 0.2)
			var temp = Get.Cam().transform.basis.x.normalized()
			var tangent = normal.cross(temp)
			aim_start = tangent
			aim_rotation = 0.0
			aim_height = 0.0
			
			Get.Cam().follow = false
			Get.UI().show_charge()
			Get.UI().charge.value = 0
			$Trail.emitting = false
			
			Get.Cam().frame_skip = true
			
		STATE_MOVING:
			Get.Cam().follow = true
			Get.UI().hide_charge()
			$Trail.emitting = true
	
	state = new_state

func shoot(direction : Vector3, strength : float) -> void:
	apply_impulse(Vector3.ZERO, direction * strength)
	angular_velocity.y += 10.0 # add some spin to the ball
	$Shot.play()

func get_planet_normal() -> Vector3:
	if not planet:
		printerr("ERROR: cannot get planet normal, no planet!")
	
	return (global_transform.origin - planet.global_transform.origin).normalized()

func get_distance_to_planet_center() -> float:
	if not planet:
		printerr("ERROR: cannot get distance to player center, no planet!")
	return (global_transform.origin - planet.global_transform.origin).length()

func _on_MinimumRestTime_timeout():
	change_state(STATE_AIMING)

func _unhandled_input(event):
	if state == STATE_AIMING:
		if event is InputEventMouseMotion:
			aim_rotation -= lerp(0, AIM_SPIN_SPEED, event.relative.x * 0.1)
			aim_rotation = wrapf(aim_rotation, 0.0, 2.0 * PI)
			aim_height -= lerp(0, AIM_SPIN_SPEED, event.relative.y * 0.1)
			aim_height = clamp(aim_height, -PI * 0.5, PI * 0.5)
	
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_UP and event.pressed:
			set_camera_zoom(camera_zoom - CAMERA_ZOOM_STEP)
			get_tree().set_input_as_handled()
		if event.button_index == BUTTON_WHEEL_DOWN and event.pressed:
			set_camera_zoom(camera_zoom + CAMERA_ZOOM_STEP)
			get_tree().set_input_as_handled()

func _on_GolfBall_body_entered(body):
	# collided
	var v = linear_velocity.length()
	if v >= IMPACT_VELOCITY_SOUND_THRESHOLD_MIN:
		if v < IMPACT_VELOCITY_SOUND_THRESHOLD_MAX:
			var r = IMPACT_VELOCITY_SOUND_THRESHOLD_MAX - IMPACT_VELOCITY_SOUND_THRESHOLD_MIN
			$Hit.volume_db = (v - IMPACT_VELOCITY_SOUND_THRESHOLD_MIN) / r * 20 - 20
		else:
			$Hit.volume_db = 0
#		$Hit.play()

func _on_ChargeTime_timeout():
	var aim = -$CamPivot.transform.basis.z
	shoot(aim, MAX_charge)
	change_state(STATE_MOVING)

func die():
	Get.Cam().clear_targets()
	queue_free()
