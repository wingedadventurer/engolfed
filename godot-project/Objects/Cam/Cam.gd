extends Camera
class_name Cam

var MOVE_LERP_WEIGHT = 0.1
var ROTATE_LERP_WEIGHT = 0.1
var FOLLOW_DISTANCE = 4.0

var target : Spatial = null
var follow_target : Spatial = null

var follow = true
var frame_skip = false

var shake_factor = 0.2
var end = false

func _process(delta):
	if frame_skip:
		frame_skip = false
		return
	
	# following player
	if follow:
		if follow_target:
			var distance_to_target = global_transform.origin.distance_to(follow_target.global_transform.origin)
			if distance_to_target > FOLLOW_DISTANCE:
				var global_destination = follow_target.global_transform.origin + (global_transform.origin - follow_target.global_transform.origin).normalized() * FOLLOW_DISTANCE
				global_transform.origin = global_transform.origin.linear_interpolate(global_destination, MOVE_LERP_WEIGHT)
			look_at(follow_target.transform.origin, transform.basis.y)
	else:
		if target:
			global_transform.origin = global_transform.origin.linear_interpolate(target.global_transform.origin, MOVE_LERP_WEIGHT)
			global_transform.basis = global_transform.basis.slerp(target.global_transform.basis, ROTATE_LERP_WEIGHT)
	
	# end effect
	if end:
		translate(Vector3.BACK * delta)
	
	# shaking
	if not $ShakeTimer.is_stopped():
		var shake_offset : Vector2 = Vector2.RIGHT * shake_factor * $ShakeTimer.time_left
		shake_offset = shake_offset.rotated(rand_range(0, 2 * PI))
		h_offset = shake_offset.x
		v_offset = shake_offset.y

func clear_targets():
	target = null
	follow_target = null

func shake(time : float, factor : float) -> void:
	shake_factor = factor
	$ShakeTimer.start(time)

func _on_ShakeTimer_timeout():
	h_offset = 0
	v_offset = 0
