extends MeshInstance

var rotate_speed = 5.0

func _process(delta):
	rotate(Vector3.UP, deg2rad(rotate_speed) * delta)
