tool
extends Spatial

var star_material = load("res://Materials/Star.tres")
var field_material = load("res://Materials/Field.tres")

var velocity_label = null

export (float) var radius = 1.0 setget set_radius
export (float) var gravity = 1.0 setget set_gravity
export (float) var gravity_radius_factor = 10.0 setget set_gravity_radius_factor
export (Color) var color = Color(1, 1, 1) setget set_color
export (Texture) var texture = null setget set_texture
export (Color) var field_color = Color(1, 1, 1) setget set_field_color

func _ready():
	$GravityAreaPulse.play("pulse")

func set_radius(value):
	radius = value
	
	if not (has_node("Sphere") and has_node("Field")):
		yield(self, "tree_entered")
	
	$Sphere.scale = Vector3.ONE * radius
	$Field.scale = Vector3.ONE * radius * gravity_radius_factor

func set_gravity(value):
	gravity = value
	$Field.gravity = gravity

func set_gravity_radius_factor(value):
	gravity_radius_factor = value
	set_radius(radius)

func set_color(value):
	color = value
	
	if not has_node("Sphere"): yield(self, "tree_entered")
	
	var material = star_material.duplicate()
	material.albedo_color = color
	material.albedo_texture = texture
	$Sphere.set_surface_material(0, material)

func set_texture(value):
	texture = value
	
	if not has_node("Sphere"): yield(self, "tree_entered")
	
	var material = star_material.duplicate()
	material.albedo_color = color
	material.albedo_texture = texture
	$Sphere.set_surface_material(0, material)

func set_field_color(value):
	field_color = value
	
	if not has_node("Field/Sphere"): yield(self, "tree_entered")
	
	var material = field_material.duplicate()
	material.albedo_color = field_color
	$Field/Sphere.set_surface_material(0, material)

func _on_Field_body_entered(body):
	if body.is_in_group("golf_ball"):
		$FieldEnter.play()

func _on_Field_body_exited(body):
	if body.is_in_group("golf_ball"):
		$FieldExit.play()

func _on_PlayerCheck_body_entered(body):
	if body.is_in_group("golf_ball"):
		body.die()
