extends Sprite3D

var EFFECT_DURATION = 1.5
var NEW_SCALE = 2.0

func _ready():
	$Tween.interpolate_property(self, "scale", Vector3.ZERO, Vector3.ONE * NEW_SCALE, EFFECT_DURATION, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.interpolate_property(self, "modulate:a", 2.0, 0.0, EFFECT_DURATION, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()

func _on_Tween_tween_completed(object, key):
	queue_free()
