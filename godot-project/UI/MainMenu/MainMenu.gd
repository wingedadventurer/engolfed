extends Control

onready var title : Control = $TitleMenu
onready var levels : Control = $LevelsMenu
onready var options : Control = $OptionsMenu
onready var credits : Control = $CreditsMenu

func _ready():
	hide_all_menus()
	title.show()
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	Music.play(Music.TYPE_MENU)

func hide_all_menus():
	title.hide()
	levels.hide()
	options.hide()
	credits.hide()

func _on_Play_pressed():
	hide_all_menus()
	levels.show()

func _on_Options_pressed():
	hide_all_menus()
	options.show()

func _on_Credits_pressed():
	hide_all_menus()
	credits.show()

func _on_Quit_pressed():
	get_tree().quit()

func _on_LevelsBack_pressed():
	hide_all_menus()
	title.show()

func _on_OptionsBack_pressed():
	hide_all_menus()
	title.show()

func _on_CreditsBack_pressed():
	hide_all_menus()
	title.show()
