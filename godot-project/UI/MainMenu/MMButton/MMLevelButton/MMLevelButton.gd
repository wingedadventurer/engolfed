extends "res://UI/MainMenu/MMButton/MMButton.gd"

export (PackedScene) var level = null

func _ready():
	if not level: disabled = true

func _on_MMLevelButton_pressed():
	if level:
		Music.play(Music.TYPE_GAME)
		get_tree().change_scene_to(level)
