extends HSlider

func _ready():
	match name:
		"Master":
			value = db2linear(AudioServer.get_bus_volume_db(0)) * 100
		"Music":
			value = db2linear(AudioServer.get_bus_volume_db(1)) * 100
		"SFX":
			value = db2linear(AudioServer.get_bus_volume_db(2)) * 100

func _on_MMSlider_value_changed(value):
	match name:
		"Master":
			AudioServer.set_bus_volume_db(0, linear2db(value * 0.01))
		"Music":
			AudioServer.set_bus_volume_db(1, linear2db(value * 0.01))
		"SFX":
			AudioServer.set_bus_volume_db(2, linear2db(value * 0.01))
