extends CheckButton

# rendering/environment/default_environment
var environment : Environment = load("res://default_env.tres")

func _ready():
	match name:
		"Fullscreen":
			pressed = OS.window_fullscreen 
		"Glow":
			pressed = environment.glow_enabled

func _on_MMCheckButton_pressed():
	$Click.play()

func _on_MMCheckButton_toggled(button_pressed):
	match name:
		"Fullscreen":
			OS.window_fullscreen = button_pressed
		"Glow":
			environment.glow_enabled = button_pressed
