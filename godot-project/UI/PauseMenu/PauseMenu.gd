extends Control

var main_menu_scene = load("res://UI/MainMenu/MainMenu.tscn")

func _ready():
	visible = false

func _process(delta):
	
	if Input.is_action_just_pressed("esc"):
		if get_tree().paused:
			get_tree().paused = false
			hide()
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			get_tree().paused = true
			show()
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_Continue_pressed():
	get_tree().paused = false
	var player = Get.Player()
	if player: player.skip_shot = true
	hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _on_Restart_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()

func _on_Menu_pressed():
	get_tree().paused = false
	get_tree().change_scene_to(main_menu_scene)

func _on_Quit_pressed():
	get_tree().quit()
