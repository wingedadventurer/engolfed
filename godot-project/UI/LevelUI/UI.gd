extends CanvasLayer
class_name UI

onready var debug_velocity : Label = $Debug/Velocity
onready var crosshair : TextureRect = $Crosshair
onready var charge : ProgressBar = $Charge

func _ready():
	charge.value = 0

func update_velocity(velocity):
	debug_velocity.text = ("Velocity: " + str(velocity))

func update_crosshair_alpha(value):
	crosshair.modulate.a = value

func update_charge_value(value):
	charge.value = value * 100

func show_charge():
	$ChargeAnim.play("show")

func hide_charge():
	$ChargeAnim.play("hide")
